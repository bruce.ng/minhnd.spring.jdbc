
package com.customer.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;


public class CustomerRowMapper implements RowMapper<Customer> {
	
	/**
	 * recommended to implement the RowMapper interface to create a custom RowMapper to suit your needs.
	 * */
	@Override
	public Customer mapRow(ResultSet rs, int rowNum)
		throws SQLException {
		Customer customer = new Customer();
		customer.setCustId(rs.getInt("CUST_ID"));
		customer.setName(rs.getString("NAME"));
		customer.setAge(rs.getInt("AGE"));
		return customer;
	}
}
