
package com.customer.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.customer.dao.CustomerDAO;
import com.customer.model.Customer;
import com.customer.model.CustomerRowMapper;

public class JdbcTemplateCustomerDAO extends JdbcTemplate implements CustomerDAO {
	
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Override
	public int insert(Customer customer) {
		String sql = "INSERT INTO CUSTOMER " + "(CUST_ID, NAME, AGE) VALUES (?, ?, ?)";
		
		jdbcTemplate = new JdbcTemplate(dataSource);
		
		int update = jdbcTemplate.update(sql, new Object[] {
			customer.getCustId(), customer.getName(), customer.getAge()
		});
		return update;
	}
	
	//insert batch example
	public int insertBatch(final List<Customer> customers) {
		
		String sql = "INSERT INTO CUSTOMER " + "(CUST_ID, NAME, AGE) VALUES (?, ?, ?)";
		
		int[] update = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps, int i)
				throws SQLException {
				Customer customer = customers.get(i);
				ps.setLong(1, customer.getCustId());
				ps.setString(2, customer.getName());
				ps.setInt(3, customer.getAge());
			}
			
			@Override
			public int getBatchSize() {
				return customers.size();
			}
		});
		
		return update.length;
	}
	
	//insert batch example with SQL
	public int insertBatchSQL(final String sql) {
		
		int[] update = jdbcTemplate.batchUpdate(new String[] {
			sql
		});
		return update.length;
		
	}
	
	@Override
	public Customer findByCustomerId(int custId) {
		String sql = "SELECT * FROM CUSTOMER WHERE CUST_ID = ?";
		
		Customer customer = (Customer) jdbcTemplate.queryForObject(sql, new Object[] {
			custId
		}, new CustomerRowMapper());
		
		return customer;
	}
	
	/**
	 * comes with a handy RowMapper implementation called ‘BeanPropertyRowMapper’,
	 * which can maps a row’s column value to a property by matching their names.
	 * Just make sure both the property and column has the same name, e.g property ‘custId’ will
	 * match to column name ‘CUSTID’ or with underscores ‘CUST_ID’.
	 * */
	public Customer findByCustomerId2(int custId) {
		
		String sql = "SELECT * FROM CUSTOMER WHERE CUST_ID = ?";
		
		Customer customer = (Customer) jdbcTemplate.queryForObject(sql, new Object[] {
			custId
		}, new BeanPropertyRowMapper<Customer>(Customer.class));
		
		return customer;
	}
	
	@Override
	public int insertNamedParameter(Customer customer) {
		return 0;
	}
	
	@Override
	public int insertBatchNamedParameter(List<Customer> customer) {
		return 0;
	}
	
	@Override
	public int insertBatchNamedParameter2(List<Customer> customer) {
		return 0;
	}
	
	@Override
	public List<Customer> findAll() {
		return null;
	}
	
	@Override
	public List<Customer> findAll2() {
		return null;
	}
	
	@Override
	public String findCustomerNameById(int custId) {
		return null;
	}
	
	@Override
	public int findTotalCustomer() {
		return 0;
	}
	
}
