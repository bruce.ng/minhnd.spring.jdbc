
package com.customer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.customer.dao.CustomerDAO;
import com.customer.model.Customer;

public class JdbcCustomerDAO implements CustomerDAO {
	
	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public int insert(Customer customer) {
		
		String sql = "INSERT INTO CUSTOMER " + "(CUST_ID, NAME, AGE) VALUES (?, ?, ?)";
		Connection conn = null;
		
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, customer.getCustId());
			ps.setString(2, customer.getName());
			ps.setInt(3, customer.getAge());
			int update = ps.executeUpdate();
			ps.close();
			return update;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	public Customer findByCustomerId(int custId) {
		
		String sql = "SELECT * FROM CUSTOMER WHERE CUST_ID = ?";
		
		Connection conn = null;
		
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, custId);
			Customer customer = null;
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				customer = new Customer(rs.getInt("CUST_ID"), rs.getString("NAME"), rs.getInt("Age"));
			}
			rs.close();
			ps.close();
			return customer;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
	}
	
	@Override
	public int insertNamedParameter(Customer customer) {
		return 0;
	}
	
	@Override
	public int insertBatch(List<Customer> customer) {
		return 0;
	}
	
	@Override
	public int insertBatchNamedParameter(List<Customer> customer) {
		return 0;
	}
	
	@Override
	public int insertBatchNamedParameter2(List<Customer> customer) {
		return 0;
	}
	
	@Override
	public int insertBatchSQL(String sql) {
		return 0;
	}
	
	@Override
	public Customer findByCustomerId2(int custId) {
		return null;
	}
	
	@Override
	public List<Customer> findAll() {
		return null;
	}
	
	@Override
	public List<Customer> findAll2() {
		return null;
	}
	
	@Override
	public String findCustomerNameById(int custId) {
		return null;
	}
	
	@Override
	public int findTotalCustomer() {
		return 0;
	}
}
