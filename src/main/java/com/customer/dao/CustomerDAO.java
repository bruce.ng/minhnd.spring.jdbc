
package com.customer.dao;

import java.util.List;

import com.customer.model.Customer;

public interface CustomerDAO {
	
	public int insert(Customer customer);
	
	public int insertNamedParameter(Customer customer);
	
	public int insertBatch(List<Customer> customer);
	
	public int insertBatchNamedParameter(List<Customer> customer);
	
	public int insertBatchNamedParameter2(List<Customer> customer);
	
	public int insertBatchSQL(String sql);
	
	public Customer findByCustomerId(int custId);
	
	public Customer findByCustomerId2(int custId);
	
	public List<Customer> findAll();
	
	public List<Customer> findAll2();
	
	public String findCustomerNameById(int custId);
	
	public int findTotalCustomer();
}
